<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class SeanceController extends Controller
{
    public function index()
    {
        // Получаем ответ из файла
        $json_response = file_get_contents(base_path('database/data/seances.json'));

        // Декодируем JSON
        $responseData = json_decode($json_response, true);

        // Данные из JSON
        $seanceDate = Carbon::createFromTimestampUTC($responseData['data']['seance_date']); // дата сеанса
        $reservedSeances = collect($responseData['data']['seances'])->sortBy(fn($s)=>$s['time']); // зарезервированное время
        $seanceLength = 45 * 60; // длительность услуги клиента в секундах
        $workingStart = $seanceDate->clone()->setTime(10, 0); // Начало рабочего дня
        $workingEnd = $seanceDate->clone()->setTime(20, 0); // конец рабочего дня
        $interval = 5 * 60; // устанавливаем минимальный интервал между слотами 5 минут

        // Создаем массив куда будем класть с доступными интервалами
        $availableSlots = [];
        $reservedSlots = [];
        foreach ($reservedSeances as $seance){
            $timeParts = explode(':', $seance['time']);
            $seanceStart =  $seanceDate->clone()->setTime($timeParts[0], $timeParts[1]);
            $seanceEnd = $seanceDate->clone()->setTime($timeParts[0], $timeParts[1])->addSeconds((int)$seance['seance_length']);
            $reservedSlots[] = ['start'=>$seanceStart, 'end'=>$seanceEnd];
        }

        // Перебираем все возможные диапазоны начиная с начала рабочего дня с интервалом 5 минут
        $slotStart = $workingStart; // начало слота
        while ($slotStart->clone()->addSeconds($seanceLength)->lessThanOrEqualTo($workingEnd)) {
            $slotEnd = $slotStart->clone()->addSeconds($seanceLength); // конец слота


            foreach ($reservedSlots as $key=>$reservedSlot) {
                // Проверяем, пересекается ли слот с уже занятыми сеансами
                if ($slotStart->lt($reservedSlot['end']) && $slotEnd->gt($reservedSlot['start'])) {
                    $slotStart = $reservedSlot['end']->clone();
                    break;
                }

                $slotStartTime = $slotStart->format('H:i');
                $nextSlot = $reservedSlots[$key + 1] ?? null;
                if($nextSlot !== null){
                    $slotEndTime = $nextSlot['start']->clone()->format('H:i');
                    $availableSlots[] = [$slotStartTime, $slotEndTime];
                    $slotStart = $nextSlot['end']->clone();
                }else if($slotStart->lte($workingEnd) && $slotEnd->lte($workingEnd)){
                    $slotEndTime = $workingEnd->clone()->format('H:i');
                    $availableSlots[] = [$slotStartTime, $slotEndTime];
                    $slotStart = $workingEnd->clone();
                }
            }

        }
        // Возвращаем результат
        return response()->json([
            'free_intervals'=>$availableSlots
        ]);

    }
}
